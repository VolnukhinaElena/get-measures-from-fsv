#!/usr/bin/python3 -B
# -*- coding: UTF-8 -*-
import argparse
import os
import sys
from os.path import dirname, abspath, normpath

import FSV_meas

filename = sys.argv[0]
default_script_dir = dirname(abspath(filename))
default_result_dir = default_script_dir + "/MeasRes"
default_type_data = "full"
default_ip = "192.168.0.248"
default_csv_fileName = 'measfromFSV.csv'
default_pic_fileName = 'screenshot.png'

parser = argparse.ArgumentParser(description='Getting picture or csv file from FSV script')
parser.add_argument('--type', type=str, default=default_type_data, help='Type of receiv data. \n'
                                                                             'For get picture use "pic",\n '
                                                                             'for get csv use "csv", \n'
                                                                             'for get full complect use "full"\n'
                                                                             '  (default: ' + default_type_data + ')')
parser.add_argument('--path2res', type=str, default=default_result_dir,
                    help='Path to result (default: ' + default_result_dir + ')')
parser.add_argument('--IP', type=str, default=default_ip, help='FSV IP (default: ' + default_ip + ')')
parser.add_argument('--csvName', type=str, default=default_csv_fileName,
                    help='CSV file name (default: ' + default_csv_fileName + ')')
parser.add_argument('--picName', type=str, default=default_pic_fileName,
                    help='Picture file name (default: ' + default_pic_fileName + ')')
args = parser.parse_args()

type_data = args.type
result_dir = args.path2res
csvName = args.csvName
picName = args.picName
ip = args.IP

csv_filePathPc = result_dir + '/' + csvName
pic_filePathPc = result_dir + '/' + picName

message = 'Get data from FSV with params: \n' \
          f'Type of data:   {type_data}\n' \
          f'Result directory:   {result_dir}\n' \
          f'IP of FSV:     {ip}'

print(message)
os.system(f'mkdir -p {result_dir}')
FSV = FSV_meas.FSV(ip=ip)

if type_data == 'csv':
    FSV.traceget(csv_file_name=csv_filePathPc)
    print(f'Full path to csv file:  {csv_filePathPc}')
elif type_data == 'pic':
    FSV.pictureget(pic_filePathPc=pic_filePathPc)
    print(f'Full path to picture file:  {pic_filePathPc}')
elif type_data == 'full':
    FSV.traceget(csv_file_name=csv_filePathPc)
    FSV.pictureget(pic_filePathPc=pic_filePathPc)
    print(f'Full path to csv file:  {csv_filePathPc}')
    print(f'Full path to picture file:  {pic_filePathPc}')
else:
    print('Worth type of data!')

print('End of process')
