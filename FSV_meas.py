# --> Import necessary packets
from RsInstrument.RsInstrument import RsInstrument
from time import sleep


class FSV:
    def __init__(self, ip):
        print('Try connect to FSV, ip address:    ' + ip)
        ressource = f'TCPIP::{ip}::5025::SOCKET'  # replace by your IP-address
        self.Instrument = RsInstrument(ressource, True, True, options="SelectVisa=socket")
        default_center_Frequency = '1580e6'
        default_RBW = '10 kHz'
        default_VBW = '50 Hz'
        default_freq_start = '10 MHz'
        default_freq_stop = '2 GHz'
        default_SPAN = '1000e6'
        default_IP = '192.168.0.248'

        # Define variables
        # ressource = 'TCPIP::192.168.0.248::5025::SOCKET'  # replace by your IP-address
        x = 0  # For counters
        loopResponse = "1"  # Variable for diverse loop requests
        Response = "0"  # Variable for diverse requests
        self.recdur = 10  # Time in seconds to find maxhold peaks

    '''
    (ressource, True, True, "SelectVisa='rs'") has the following meaning:
    (VISA-resource, id_query, reset, options)
    - id_query: if True: the instrument's model name is verified against the models 
    supported by the driver and eventually throws an exception.   
    - reset: Resets the instrument (sends *RST) command and clears its status syb-system
    - option SelectVisa:
                - 'SelectVisa = 'socket' - uses no VISA implementation for socket connections - you do not need any VISA-C installation
                - 'SelectVisa = 'rs' - forces usage of Rohde&Schwarz Visa
                - 'SelectVisa = 'ni' - forces usage of National Instruments Visa     
    '''

    ##
    ## Define subroutines
    ##

    def comprep(self):
        """Preparation of the communication (termination, timeout, etc...)"""

        print(f'VISA Manufacturer: {self.Instrument.visa_manufacturer}')  # Confirm VISA package to be choosen
        self.Instrument.visa_timeout = 5000  # Timeout in ms for VISA Read Operations
        self.Instrument.opc_timeout = 3000  # Timeout in ms for opc-synchronised operations
        self.Instrument.instrument_status_checking = True  # Error check after each command
        self.Instrument.clear_status()  # Clear status register

    def close(self):
        """Close the VISA session"""
        self.Instrument.close()

    def comcheck(self):
        """Check communication with the device by requesting it's ID"""
        # Just knock on the door to see if instrument is present
        idnResponse = self.Instrument.query_str('*IDN?')
        sleep(1)
        print('Hello, I am ' + idnResponse)

    def measprep(self, Center_Frequency, SPAN, RBW='10 kHz', VBW='50 Hz', sweep_point=32000):
        """Prepare instrument for desired measurements
        - Set Trace to Max Hold (and Positive Peak automatically)
        """
        self.Instrument.write_str_with_opc('FREQuency:CENTer ' + Center_Frequency)  # Center Frequency to 2450 MHz
        self.Instrument.write_str_with_opc('FREQuency:SPAN ' + SPAN)  # SPAN is 100 MHz now
        self.Instrument.write_str_with_opc(f'BAND {RBW}')  # 10 kHz
        self.Instrument.write_str_with_opc(f'BAND:VID {VBW}')  # 50 Hz
        self.Instrument.write_str_with_opc(f'SWE:POIN {sweep_point}')  # 50 Hz
        self.Instrument.write_str_with_opc('DISPlay:TRACe1:MODE MAXHold')  # Trace to Max Hold

    def traceget(self, csv_file_name):
        """Initialize continuous measurement, stop it after 10 seconds and query trace data"""
        self.Instrument.write_str_with_opc('INITiate:CONTinuous ON')  # Continuous measurement on trace 1 ON
        sleep(int(self.recdur))  # Wait for preset record time
        self.Instrument.write_str_with_opc('INITiate:CONTinuous OFF')  # Continuous measurement on trace 1 OFF
        TraceData = self.Instrument.query_str('Trace:DATA? TRACe1')  # Read y data of trace 1
        TracePoints = self.Instrument.query_str('TRACe:DATA:X? Trace1')  # Read the corresponding x data
        NoSweepPoints = self.Instrument.query_str('SWEep:POINts?')  # Request for the number of sweep points

        CSVTraceData = TraceData.split(",")  # Slice the amplitude list into a tuple
        CSVTracePoints = TracePoints.split(",")  # Slice the frequency list into a tuple

        # now file writing begins
        file = open(csv_file_name, 'w')  # Open File for writing
        file.write("Frequency in Hz;Power in dBm\n")  # Write the headline
        x = 0  # Set counter to 0 as touple does not begin with 1
        while x < int(NoSweepPoints):  # Perform loop until all sweep points are covered
            file.write(CSVTracePoints[x])  # Write amplitude measurement data
            file.write(";")  # Add semicolon as CSV separator
            file.write(CSVTraceData[x])  # Write frequency data
            file.write("\n")  # Add a new line control character
            x = x + 1  # Increment counter
        file.close()  # CLose the file

    def pictureget(self, pic_filePathPc, pic_filePathInstr=fr"c:\Temp\pic.png"):
        self.Instrument.write_str_with_opc('INITiate:CONTinuous ON')  # Continuous measurement on trace 1 ON
        # turns on color printing
        self.Instrument.write_str_with_opc('HCOP:DEV:COL ON')
        # select file format
        # (WMF | GDI | EWMF | BMP | PNG | JPEG | JPG | PDF | SVG | DOC | RTF)
        self.Instrument.write_str_with_opc('HCOP:DEV:LANG PNG')
        # set print to file
        self.Instrument.write_str_with_opc("HCOP:DEST 'MMEM'")
        # file path on instrument
        self.Instrument.write_str_with_opc(f'MMEM:NAME "{pic_filePathInstr}"')
        # create screenshot
        self.Instrument.write_str_with_opc('HCOP:IMM')

        self.Instrument.read_file_from_instrument_to_pc(pic_filePathInstr, pic_filePathPc)
        # ask for file data from instrument


