# get measures from FSV

Sometimes i will add some useful functions for communication with R&S FSV, as setup, getting data, etc (worked on linux). 


Basic for script I take from [this](https://github.com/Rohde-Schwarz/Examples) repository, and then just add some changes.

Used equipments:

![](stuff/text17556.png)

# getFSVmeas.py

For getting csv file and\or screenshot:

`python3 getFSVmeas.py --help`

You can config:

- path to saved data, 

- FSV IP, 

- type of getting data: only csv, only png, or full comlect, 

- name of saved file



As result, you have a picture from FSV:
![](stuff/Full_Power_CentrFreq_1200e6_SPAN_100e6_Spectr.png)

And/or spectrum in .csv format (you can see it in stuff/Full_Power_CentrFreq_1200e6_SPAN_100e6_SpectrTraceFile.CSV)
## FSV.py

Python class for all features
